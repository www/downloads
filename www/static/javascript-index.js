// Original height of the message box
var originalHeight;

// Execute upon document ready
$(function() {
	// Wrap everything in a container
	$("body").html(
		'<div class="content">' +
			'<div class="wrapper">' +
				$("body").html() +
			'</div>' +
		'</div>'
	);
	
	// Generate a page header
	$("body").prepend(
		'<div class="top-line">' +
			'<div class="wrapper">' +
				'<a href="/" class="logo">Qt Downloads</a>' +
				'<ul class="main-menu">' +
					'<li><a href="http://qt-project.org/">Qt Home</a></li>' +
					'<li><a href="https://bugreports.qt-project.org/">Bug Tracker</a></li>' +
					'<li><a href="https://codereview.qt-project.org/">Code Review</a></li>' +
					'<li><a href="http://planet.qt-project.org">Planet Qt</a></li>' +
					'<li class="active"><a href="http://downloads-dev.qt-project.org">Qt Downloads</a></li>' +
				'</ul>' +
				'<fieldset>' +
					'<form id="search_form" method="get" action="http://qt-project.org/search">' +
						'<input type="text" name="search">' +
						'<button>Search</button>' +
					'</form>' +
				'</fieldset>' +
			'</div>' +
		'</div>'
	);

	// Generate a page footer
	// $("address").html('');

	// Do these only if the folder has a message text
	if ($("pre").length > 0) {
		// Replace the code with a alert box
		var lines = $("pre").text().trim("\n").split("\n");

		if (lines.length > 1) {
			$("pre").replaceWith(
				'<div class="alert alert-info">' +
					'<div class="message-toggle pull-right">' +
						'<i class="icon-chevron-down"></i>' +
					'</div>' +
					'<div class="message-title hide">' +
						'<i class="icon-info-sign"></i> Information about this folder' +
					'</div>' +
					'<pre class="message">' +
						$("pre").html().trim("\n") +
					'</pre>' +
					'<div class="message-metadata hide"></div>' +
				'</div>'
			);
		} else {
			$("pre").replaceWith(
				'<div class="alert alert-info">' +
					'<pre><i class="icon-info-sign"></i> ' + $("pre").html().trim("\n") + '</pre>' +
				'</div>'
			);

			$(".alert-info").css("minHeight", 20);
		}

		// Save the original height
		originalHeight = $(".message").innerHeight();

		// Collapse on startup
		$(".message-title").show();
		$(".message").css({
			opacity: 0,
			height: 0,
			display: "none"
		});

		// Toggle container visibility
		$(".message-toggle").click(function() {
			if ($(".message").is(":visible")) {
				$(".message").animate({
					height: 0,
					opacity: 0
				}, function() {
					$(this).hide();
				});


				$(".message-title").fadeIn();
				$(".message-toggle i")
					.removeClass("icon-chevron-up")
					.addClass("icon-chevron-down");
			} else {
				$(".message").show();
				$(".message").animate({
					height: originalHeight,
					opacity: 1
				});
				$(".message-title").fadeOut();
				$(".message-toggle i")
					.removeClass("icon-chevron-down")
					.addClass("icon-chevron-up");
			}
		});
	}

	// Add icons to files/folders
	$("tbody tr").each(function() {
		if ($(this).children().last().text().trim().length == 0) {
			$(this).children().eq(1).prepend(
				'<i class="icon-folder-close"></i> '
			);
		} else if ($(this).children().last().text() != "Metadata" ) {
			$(this).children().eq(1).prepend(
				'<i class="icon-file"></i> '
			);
		}

		if ($(this).children().eq(1).text().trim().toLowerCase() == "parent directory") {
			var html = $(this).children().eq(1).html().replace("icon-folder-close", "icon-arrow-up");
			$(this).children().eq(1).html(html);
		}
	});

	$("address").html(
		'<p>Powered by <a href="http://mirrorbrain.org/">MirrorBrain</a> - Maintained by ' +
		'<a href="mailto:sysadmin@qt-project.org">Qt Project Sysadmin</a> - ' +
		'<br />Based on download.kde.org created by KDE Sysadmin.</p>'
	);
});
