<VirtualHost *:80>
        ServerName downloads.qt-project.org
        ServerAlias downloads-dev.qt-project.org
        ServerAdmin sysadmin@qt-project.org
        DocumentRoot /srv/downloads

        # Apply some styling
        MirrorBrainMetalinkPublisher "Qt Project" http://downloads.qt-project.org
        MirrorBrainMirrorlistHeader "/var/www/static/header.html"
        MirrorBrainMirrorlistFooter "/var/www/static/footer.html"
        IndexOptions HTMLTable SuppressHTMLPreamble FancyIndexing XHTML FoldersFirst SuppressDescription Metalink MirrorList VersionSort IconWidth=16 IconHeight=16 NameWidth=* #SuppressColumnsorting
        HeaderName .message
                                                                                                                                     
        <Files ~ "(.message)$">                                                                                                      
                ForceType text/plain 
        </Files>                                                                                                                     
                                                                                                                                     
        <Directory />                                                                                                                
                Options FollowSymLinks                                                                                               
                AllowOverride None                                                                                                   
        </Directory>                                                                                                                 
        <Directory /srv/downloads/>                                                                                                  
                # Use mirrorbrain                                                                                                    
                MirrorBrainEngine On                                                                                                 
                MirrorBrainDebug Off                                                                                                 
                                                                                                                                     
                # Enable .mirrorlist and friends                                                                                     
                                                                                                                                     
                FormGET On                                                                                                           
                MirrorBrainHandleHEADRequestLocally Off                                                                              
                MirrorBrainMinSize 2048                                                                                              
                MirrorBrainExcludeFileMask "\.(md5|sha1|asc)"                                                                        
                MirrorBrainMinSize 0                                                                                                 
                MirrorBrainExcludeUserAgent rpm/4.4.2*                                                                               
                MirrorBrainExcludeUserAgent *APT-HTTP*                                                                               
                MirrorBrainExcludeMimeType application/pgp-keys                                                                      
                IndexOrderDefault Descending Name                                                                                    
                IndexHeadInsert "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"><link rel=\"stylesheet\" type=\"text/css\" href=\"/static/normalize.css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"/static/style.css\"><link rel=\"stylesheet\" href=\"/static/bootstrap.css\" type=\"text/css\"><link rel=\"stylesheet\" href=\"/static/bootstrap-download-index.css\" type=\"text/css\"><script type=\"text/javascript\" src=\"/static/jquery.js\"></script> <script type=\"text/javascript\" src=\"/static/javascript-index.js\"></script>"

                # Turn on ASN redirection
                ASLookup On
                ASSetHeaders On

                Options FollowSymLinks Indexes
                AllowOverride None
                Order allow,deny
                Allow from all
        </Directory>

        ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
        <Directory "/usr/lib/cgi-bin">
                AllowOverride None
                Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
                Order allow,deny
                Allow from all
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/error.log

        # Possible values include: debug, info, notice, warn, error, crit,
        # alert, emerg.
        LogLevel info 

        LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" \
                want:%{WANT}e give:%{GIVE}e r:%{MB_REALM}e %{X-MirrorBrain-Mirror}o \
                %{MB_CONTINENT_CODE}e:%{MB_COUNTRY_CODE}e ASN:%{ASN}e P:%{PFX}e \
                size:%{MB_FILESIZE}e %{Range}i" combined_redirect


        CustomLog ${APACHE_LOG_DIR}/access.log combined_redirect
</VirtualHost>
